#!/usr/local/bin/perl -s
use strict;
use warnings;

our $no_bak;
our $no_report;
our $v;
sub v { print join "\n" => @_, '' if $v }

use Data::Dumper;
use lib 'perlib';
use Text::CSV_PP;
use My::System qw( mysystem );

my $root_dir = "/ss/dbs/stats";
my $makebak = "/galileo/stats/scripts/makebak";

our $env;  $env ||= 'prod';

my %hosts = (
    prod    => "https://admin.galileo.usg.edu",
    staging => "https://ga-staging.galileo.usg.edu",
);

my $report_url = "$hosts{ $env }/vendor_statistics_identifier_for_stats/crwl";
my $Inst_key_file = "$root_dir/stats/westlaw_vendor_statistics_identifier.csv";

#---------------------------------------------------------------------
main: {

# debug
$v         //= 1;
$no_bak    //= 1;
$no_report //= 1;

westlaw_fetch_report( $report_url, $Inst_key_file ) unless $no_report;
westlaw_stats_build();

exit;
}

#---------------------------------------------------------------------
sub westlaw_fetch_report {
    my( $report_url, $Inst_key_file ) = @_;
    my $wget = "/usr/bin/wget";
    mysystem( "$wget --no-check-certificate -O $Inst_key_file $report_url" );
}

#---------------------------------------------------------------------
sub westlaw_stats_build {

    my $citation = "stats_monthly_westlaw_citation_data";
    my $search   = "stats_monthly_westlaw_search_data";
    my $fulltext = "stats_monthly_westlaw_fulltext_data";

    my %new_files = (
        citation => "$root_dir/stats/${citation}_new",
        search   => "$root_dir/stats/${search}_new",
        fulltext => "$root_dir/stats/${fulltext}_new",
    );

    my %temp_files = (
        citation => "$root_dir/stats/temp_${citation}",
        search   => "$root_dir/stats/temp_${search}",
        fulltext => "$root_dir/stats/temp_${fulltext}",
    );

    my $raw_data_dir  = "$root_dir/ftp/galileo_stats/westlaw";
    my @data_files    = glob("$raw_data_dir/*");

    my %stats_type_lookup = (
        "MULTI-SEARCH KEYCITE"                    => "citation",
        "MULTI-SEARCH TRANSACTIONAL SEARCHES"     => "search",
        "MULTI-SEARCH WN PR SEARCHES 3"           => "search",
        "MULTI-SEARCH DOCUMENT DISPLAYS"          => "fulltext",
        "NEWSROOM MULTI-SEARCH DOCUMENT DISPLAYS" => "fulltext",
        "MULTI-SEARCH WN PR REPORTS 03"           => "fulltext",
        "MULTI-SEARCH ONLINE IMAGES"              => "fulltext",
        "MULTI-SEARCH WN DOC DISPLAY 20"          => "fulltext",
    );

    my %stats_types = (
        citation => "D",
        search   => "S",
        fulltext => "F",
    );
    my $server_designation  = "W";  # for westlaw, i.e., not really a server
    my $resource_code       = "CRWL";

    my %inst_codes;
    open my $fh, '<', $Inst_key_file or die "Can't open $Inst_key_file: $!";
    my $_headers = <$fh>;
    while( my $line = <$fh> ) {
        my( $inst_code, $_dbs_code, $account_number ) = csv_split( $line );
        $inst_codes{ $account_number } = $inst_code if $account_number;
    }
    close $fh;

    unless( $no_bak ) {
        for my $type ( qw( citation search fulltext ) ) {
            my $file = $new_files{ $type };
            system( $makebak, $file ) if -s $file;
        }
    }

    my %counts;
    for my $file( @data_files ) {
        print "loading data from $file\n";

        # get date from files
        my( $date ) = $file =~ m{(\d{6})};
        die "no date: $file" unless $date;

        $date = "m$date";

	open my $csv_fh, '<', $file or die "Can't open $file: $!";
        my $_headers = <$csv_fh>;
        while( my $line = <$csv_fh> ) {
            my( $account_number, $description, $count ) = csv_split( $line );

            my $inst_code = $inst_codes{ $account_number };
            die "no inst code: $line" unless $inst_code;

            my $stats_type = $stats_type_lookup{ $description };
            die "no stats type: $line" unless $stats_type;

            $counts{ $stats_type }{ $date }{ $inst_code } += $count
        }
        close $csv_fh;
    }

    for my $stats_type ( sort keys %counts ) {
        v $stats_type;
        my $temp_file = $temp_files{ $stats_type };

        open my $fh, '>', $temp_file or die "Can't open $temp_file: $!";
        for my $date ( sort keys %{$counts{ $stats_type }} ) {
            v $date;
            for my $inst_code ( sort keys %{$counts{ $stats_type }{ $date }} ) {
                my $count = $counts{ $stats_type }{ $date }{ $inst_code };
                my $line_out = join " " =>
                    $date,
                    uc($inst_code),
                    $server_designation,
                    $stats_types{ $stats_type },
                    $resource_code,
                    $count;
                print $fh $line_out."\n";
            }
        }
        close $fh;
        my $new_file = $new_files{ $stats_type };
	`touch $new_file` unless -e $new_file;
	`sort -o $temp_file $temp_file`;
	`sort -m -o $new_file $new_file $temp_file`;
    }

}

#---------------------------------------------------------------------
{ my $csv;
sub csv_split {
    my( $in ) = @_;

    $csv = Text::CSV_PP->new( { binary => 1 } ) unless $csv;
    $csv->parse( $in ) or die( "Can't parse line: ($in)".$csv->error_diag() );
    $csv->fields();  # returned
}}

__END__
