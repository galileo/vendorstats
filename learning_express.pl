#!/usr/local/bin/perl -s
use strict;
use warnings;

our $no_bak;
our $v;
sub v { print join "\n" => @_, '' if $v }

use Data::Dumper;
use lib 'perlib';
use Text::CSV_PP;
use My::System qw( mysystem );

my $root_dir = "/ss/dbs/stats";
my $makebak = "/galileo/stats/scripts/makebak";

our $env;  $env ||= 'prod';

#---------------------------------------------------------------------
main: {

# debug
$v         //= 1;
# $no_bak    //= 1;

learning_express_stats_build();

exit;
}

#---------------------------------------------------------------------
sub learning_express_stats_build {

    my $sessions = "stats_monthly_learning_express_sessions_data";
    my $fulltext = "stats_monthly_learning_express_fulltext_data";

    my %new_files = (
        sessions => "$root_dir/stats/${sessions}_new",
        fulltext => "$root_dir/stats/${fulltext}_new",
    );

    my %temp_files = (
        sessions => "$root_dir/stats/temp_${sessions}",
        fulltext => "$root_dir/stats/temp_${fulltext}",
    );

    my $raw_data_dir = "$root_dir/ftp/galileo_stats/learning_express";    
    my @data_files   = glob("$raw_data_dir/*");

    my %stats_types = (
        sessions => "A",
        fulltext => "F",
    );
    my $server_designation = "X";  # for learning express, i.e., not really a server
    my $resource_code      = "ZXLE";

    unless( $no_bak ) {
        for my $type ( qw( sessions fulltext ) ) {
            my $file = $new_files{ $type };
            system( $makebak, $file ) if -s $file;
        }
    }

    my %counts;
    for my $file( @data_files ) {
        print "loading data from $file\n";

        # get date from files
        my( $date ) = $file =~ m{(\d{6})};
        die "no date: $file" unless $date;

        $date = "m$date";

        open my $csv_fh, '<', $file or die "Can't open $file: $!";
        my $headers = <$csv_fh>;
        my $stats_type = ($headers =~ /Sessions/) ? 'sessions' : 'fulltext';
        if( $stats_type eq 'sessions' ) {
            # headers: Institution,System,Customer Institution ID,Sessions
            while( my $line = <$csv_fh> ) {
                my( undef, undef, $inst_code, $count ) = csv_split( $line );
                $inst_code = strip( $inst_code );
                die "no inst code: $line" if $count and not $inst_code;
                $count =~ s{,}{}g;

                next unless $count;

                $counts{ $stats_type }{ $date }{ $inst_code } += $count
            }
        }
        else {
            # headers: Institution Name,System,Customer Institution ID,# Page Hits,Total # of Resources
            while( my $line = <$csv_fh> ) {
                my( undef, undef, $inst_code, $page_count, $resource_count ) = csv_split( $line );
                $inst_code = strip( $inst_code );
                die "no inst code: $line" if ($page_count or $resource_count) and not $inst_code;
                $page_count     =~ s{,}{}g;
                $resource_count =~ s{,}{}g;
                my $count = $page_count + $resource_count;
                next unless $count;

                $counts{ $stats_type }{ $date }{ $inst_code } += $count
            }
        }
        close $csv_fh;
    }

    for my $stats_type ( sort keys %counts ) {
        v $stats_type;
        my $temp_file = $temp_files{ $stats_type };

        open my $fh, '>', $temp_file or die "Can't open $temp_file: $!";
        for my $date ( sort keys %{$counts{ $stats_type }} ) {
            v $date;
            for my $inst_code ( sort keys %{$counts{ $stats_type }{ $date }} ) {
                my $count = $counts{ $stats_type }{ $date }{ $inst_code };
                my $line_out = join " " =>
                    $date,
                    uc($inst_code),
                    $server_designation,
                    $stats_types{ $stats_type },
                    $resource_code,
                    $count;
                print $fh $line_out."\n";
            }
        }
        close $fh;
        my $new_file = $new_files{ $stats_type };
        `touch $new_file` unless -e $new_file;
        `sort -o $temp_file $temp_file`;
        `sort -m -o $new_file $new_file $temp_file`;
    }

}

#---------------------------------------------------------------------
sub strip {
  my ( $s ) = @_;
  for( $s ) {
    s{^\s+}{}g;
    s{\s+$}{}g;
  }
  $s
}

#---------------------------------------------------------------------
{ my $csv;
sub csv_split {
    my( $in ) = @_;

    $csv = Text::CSV_PP->new( { binary => 1 } ) unless $csv;
    $csv->parse( $in ) or die( "Can't parse line: ($in)".$csv->error_diag() );
    $csv->fields();  # returned
}}

__END__
