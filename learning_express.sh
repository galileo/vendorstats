#!/bin/bash

echo "--- Loading these files: ---"
      ls -la ../ftp/galileo_stats/learning_express/*.csv
echo "----------------------------"

echo "ls -la stats_monthly_learning_express_*data"
      ls -la stats_monthly_learning_express_*data

echo "ls -la stats_monthly_learning_express_*data_new"
      ls -la stats_monthly_learning_express_*data_new

# sessions

echo "tail stats_monthly_learning_express_sessions_data"
      tail stats_monthly_learning_express_sessions_data
echo "tail stats_monthly_learning_express_sessions_data_new"
      tail stats_monthly_learning_express_sessions_data_new

# fulltext

echo "tail stats_monthly_learning_express_fulltext_data"
      tail stats_monthly_learning_express_fulltext_data
echo "tail stats_monthly_learning_express_fulltext_data_new"
      tail stats_monthly_learning_express_fulltext_data_new

echo "--- Loading these files: ---"
      ls -la ../ftp/galileo_stats/learning_express/*.csv
echo "----------------------------"

echo "ls -la stats_monthly_learning_express_*data"
      ls -la stats_monthly_learning_express_*data

echo "ls -la stats_monthly_learning_express_*data_new"
      ls -la stats_monthly_learning_express_*data_new

while true; do
    read -p "Run? :" yn
    case $yn in
        [Yy]* ) ./learning_express.pl; break;;
        [Nn]* ) exit;;
        * ) echo "Please answer yes or no.";;
    esac
done

# sessions

echo "diff stats_monthly_learning_express_sessions_data_new stats_monthly_learning_express_sessions_data"
      diff stats_monthly_learning_express_sessions_data_new stats_monthly_learning_express_sessions_data
echo "cp   stats_monthly_learning_express_sessions_data_new stats_monthly_learning_express_sessions_data"
      cp   stats_monthly_learning_express_sessions_data_new stats_monthly_learning_express_sessions_data

echo "tail stats_monthly_learning_express_sessions_data"
      tail stats_monthly_learning_express_sessions_data
echo "tail stats_monthly_learning_express_sessions_data_new"
      tail stats_monthly_learning_express_sessions_data_new

# fulltext

echo "diff stats_monthly_learning_express_fulltext_data_new stats_monthly_learning_express_fulltext_data"
      diff stats_monthly_learning_express_fulltext_data_new stats_monthly_learning_express_fulltext_data
echo "cp   stats_monthly_learning_express_fulltext_data_new stats_monthly_learning_express_fulltext_data"
      cp   stats_monthly_learning_express_fulltext_data_new stats_monthly_learning_express_fulltext_data

echo "tail stats_monthly_learning_express_fulltext_data"
      tail stats_monthly_learning_express_fulltext_data
echo "tail stats_monthly_learning_express_fulltext_data_new"
      tail stats_monthly_learning_express_fulltext_data_new

echo "ls -la stats_monthly_learning_express_*data"
      ls -la stats_monthly_learning_express_*data

echo "ls -la stats_monthly_learning_express_*data_new"
      ls -la stats_monthly_learning_express_*data_new

