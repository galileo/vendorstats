#!/usr/local/bin/perl -s
use strict;
use warnings;

our $no_bak;
our $no_report;
our $v;
sub v { print join "\n" => @_, '' if $v }

use Data::Dumper;
use lib 'perlib';
use Text::CSV_PP;
use My::System qw( mysystem );

my $root_dir = "/ss/dbs/stats";
my $makebak = "/galileo/stats/scripts/makebak";

#---------------------------------------------------------------------
main: {

# debug
$v         //= 1;
$no_bak    //= 1;
$no_report //= 1;

sage_stats_build();

exit;
}

#---------------------------------------------------------------------
sub sage_fetch_report {
    my( $report_url, $Inst_key_file ) = @_;
    my $wget = "/usr/bin/wget";
    mysystem( "$wget --no-check-certificate -O $Inst_key_file $report_url" );
}

#---------------------------------------------------------------------
sub sage_stats_build {

    my $fulltext = "stats_monthly_sage_fulltext_data";

    my %new_files = (
        fulltext => "$root_dir/stats/${fulltext}_new",
    );

    my %temp_files = (
        fulltext => "$root_dir/stats/temp_${fulltext}",
    );

    my $raw_data_dir  = "$root_dir/ftp/galileo_stats/sage";
    my @data_files    = glob("$raw_data_dir/*");

    my %resource_codes = (
        '2016 Annual Collection' => 'sag2',
        '2017 Annual Collection' => 'sag2',
        '2018 Annual Collection' => 'sag2',
        '2019 Annual Collection' => 'sag2',
        '2020 Annual Collection' => 'sag2',
        '2021 Annual Collection' => 'sag2',
        '2022 Annual Collection' => 'sag2',
        '2023 Annual Collection' => 'sag2',
        '2024 Annual Collection' => 'sag2',
        'SRM Business'           => 'sag1',
        'SRM Foundations'        => 'sag1',
    );

    my $stats_type = 'fulltext';  # only fulltext currently

    my %stats_types = (
        fulltext => "F",
    );

    my $server_designation  = "K";  # for sage, i.e., not really a server

    my %inst_codes = (
        'Abraham Baldwin Agricultural College'  =>  'ABR1',
        'Albany State University'               =>  'ALB1',
        'Augusta University'                    =>  'AUG1',
        'Clayton State University'              =>  'CLA1',
        'College of Coastal Georgia'            =>  'BRU1',
        'Columbus State University'             =>  'COL1',
        'Fort Valley State University'          =>  'FOR1',
        'Georgia College and State University'  =>  'GEO1',
        'Georgia Gwinnett College'              =>  'GWIN',
        'Georgia Highlands College'             =>  'FLO1',
        'Georgia Institute of Technology'       =>  'GIT1',
        'Georgia Southern University'           =>  'GSO1',
        'Georgia Southwestern State University' =>  'GSW1',
        'Georgia State University'              =>  'GSU1',
        'Kennesaw State University'             =>  'KEN1',
        'Middle Georgia State University'       =>  'MGA1',
        'Savannah State University'             =>  'SAV1',
        'University of Georgia'                 =>  'UGA1',
        'University of North Georgia'           =>  'NGA1',
        'University of West Georgia'            =>  'WGC1',
        'University System of Georgia'          =>  'OIT1',
        'Valdosta State University'             =>  'VAL1',
    );

    unless( $no_bak ) {
        for my $type ( qw( fulltext ) ) {
            my $file = $new_files{ $type };
            system( $makebak, $file ) if -s $file;
        }
    }

    my %counts;
    for my $file( @data_files ) {
        print "loading data from $file\n";

        # get date from files
        my( $date ) = $file =~ m{(\d{6})};
        die "no date: $file" unless $date;

        $date = "m$date";

	open my $csv_fh, '<', $file or die "Can't open $file: $!";
        my $_headers = <$csv_fh>;
        while( my $line = <$csv_fh> ) {
            my( $inst_name, $sage_product, $count ) = csv_split( $line );
            next unless $count;

            my $inst_code = $inst_codes{ $inst_name };
            die "no inst code: $line" unless $inst_code;

            my $resource_code = $resource_codes{ $sage_product };

            $counts{ $stats_type }{ $date }{ $inst_code }{ $resource_code } += $count
        }
        close $csv_fh;
    }

    for my $stats_type ( sort keys %counts ) {
        v $stats_type;
        my $temp_file = $temp_files{ $stats_type };

        open my $fh, '>', $temp_file or die "Can't open $temp_file: $!";
        for my $date ( sort keys %{$counts{ $stats_type }} ) {
            v $date;
            for my $inst_code ( sort keys %{$counts{ $stats_type }{ $date }} ) {
                for my $resource_code ( sort keys %{$counts{ $stats_type }{ $date }{ $inst_code }} ) {
                    my $count = $counts{ $stats_type }{ $date }{ $inst_code }{ $resource_code };
                    my $line_out = join " " =>
                        $date,
                        uc($inst_code),
                        $server_designation,
                        $stats_types{ $stats_type },
                        uc($resource_code),
                        $count;
                    print $fh $line_out."\n";
                }
            }
        }
        close $fh;
        my $new_file = $new_files{ $stats_type };
	`touch $new_file` unless -e $new_file;
	`sort -o $temp_file $temp_file`;
	`sort -m -o $new_file $new_file $temp_file`;
    }

}

#---------------------------------------------------------------------
{ my $csv;
sub csv_split {
    my( $in ) = @_;

    $csv = Text::CSV_PP->new( { binary => 1 } ) unless $csv;
    $csv->parse( $in ) or die( "Can't parse line: ($in)".$csv->error_diag() );
    $csv->fields();  # returned
}}

__END__
