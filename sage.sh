#!/bin/bash

echo "ls -la stats_monthly_sage_*data"
      ls -la stats_monthly_sage_*data

echo "ls -la stats_monthly_sage_*data_new"
      ls -la stats_monthly_sage_*data_new

# fulltext

echo "tail stats_monthly_sage_fulltext_data"
      tail stats_monthly_sage_fulltext_data
echo "tail stats_monthly_sage_fulltext_data_new"
      tail stats_monthly_sage_fulltext_data_new

while true; do
    read -p "Run? :" yn
    case $yn in
        [Yy]* ) ./sage.pl; break;;
        [Nn]* ) exit;;
        * ) echo "Please answer yes or no.";;
    esac
done

# fulltext

echo "diff stats_monthly_sage_fulltext_data_new stats_monthly_sage_fulltext_data"
      diff stats_monthly_sage_fulltext_data_new stats_monthly_sage_fulltext_data
echo "cp   stats_monthly_sage_fulltext_data_new stats_monthly_sage_fulltext_data"
      cp   stats_monthly_sage_fulltext_data_new stats_monthly_sage_fulltext_data

echo "tail stats_monthly_sage_fulltext_data"
      tail stats_monthly_sage_fulltext_data
echo "tail stats_monthly_sage_fulltext_data_new"
      tail stats_monthly_sage_fulltext_data_new

echo "ls -la stats_monthly_sage_*data"
      ls -la stats_monthly_sage_*data

echo "ls -la stats_monthly_sage_*data_new"
      ls -la stats_monthly_sage_*data_new

